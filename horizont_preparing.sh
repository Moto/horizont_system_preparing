#!/bin/bash

#############################################################
#		Preparation system for Horizont installation
#
#		      Latest update: 27 aug 2019
#
#    Initial release : Krasheninnikov Anton
#
#    Description
#    ============
#
#    This script automates system preparation for Horizont 
#    installationon in Astra and Debian Linux.
#
#############################################################

if [ $(id -u) -ne 0 ]; then
    echo "///////////////////////////////////////////////"
    echo "///////     =!!=   Warning    =!!=      ///////"
    echo "/////// The script must be run as root. ///////"
    echo "///////////////////////////////////////////////"
    exit
fi
# >>>> Add new group <<<<
groupadd horiz -g 4001
if [ "$?" -ne "0" ]; then
    echo "=!!= Group add: ERROR."
    exit
fi
echo "+ Group add: complete."
# >>>> Add new user <<<<
useradd horizont --shell /bin/bash --home /usr/PROZESS --uid 5001 \
                 --groups adm,cdrom,audio,dip,video,plugdev,netdev,\
astra-console,astra-admin,horiz
if [ "$?" -ne "0" ]; then
    echo "=!!= User add: ERROR."
    exit
fi
echo "+ User add: complete."

password_set_flag=1
while [ "$password_set_flag" -ne 0 ];
do
  echo "Please, enter a password for horizont user."
  passwd horizont
  password_set_flag=$?
  if [ "$?" -ne "0" ]; then
      echo "=!!= Change horizont user pass: ERROR."
      exit
  fi
done

# >>>> Change own folder /usr/PROZESS <<<<
chown -R horizont:horiz /usr/PROZESS
if [ "$?" -ne "0" ]; then
    echo "=!!= Change own /usr/PROZESS folder: ERROR."
    exit
fi
echo "+ Change own on /usr/PROZESS folder: complete."
# >>>> Update apt package list <<<<
apt -qq update
if [ "$?" -ne "0" ]; then
    echo "=!!= Update package list: ERROR."
    exit
fi
echo "+ Package list update: complete."
# >>>> Install needfull packages <<<<
apt install dirmngr python-dev python-pip snmptrapd ssh ntp curl rsync \
                libaio1 ifenslave nmap minicom --yes
if [ "$?" -ne "0" ]; then
    echo "=!!= Install dirmngr, python-dev, snmptrapd and etc... ERROR."
    exit
fi
echo "+ Complete install: dirmngr, python-dev, snmptrapd and etc."
# >>>> Add Debian repos keys <<<<
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EF0F382A1A7B6500 &&
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7638D0442B90D010
if [ "$?" -ne "0" ]; then
    echo "=!!= Add Debian repos keys: ERROR."
    exit
fi
echo "+ Repos key added: complete."
# >>>> Add repos in sources list <<<<
sh -c "echo '###### Debian Main Repos' >> /etc/apt/sources.list" &&
sh -c "echo 'deb http://mirror.yandex.ru/debian/ stretch main' >> /etc/apt/sources.list" &&
sh -c "echo 'deb http://mirror.yandex.ru/debian/ jessie main' >> /etc/apt/sources.list"
if [ "$?" -ne "0" ]; then
    echo "=!!= Add Debian repos in sources.list: ERROR."
    exit
fi
echo "+ Add Debian repos in sources.list: complete."
# >>>> Update package list <<<<
apt update
if [ "$?" -ne "0" ]; then
    echo "=!!= Update package list: ERROR."
    exit
fi
echo "+ Update package list: complete."
# >>>> Install special library for Horizont  <<<<
apt install libicu52 libboost-regex-dev libgl1-mesa-glx \
                libxmu6 libxrender1 libxrandr2 libxcursor1 libxinerama1 \
                libxft2 libxml2 --yes
if [ "$?" -ne "0" ]; then
    echo "=!!= Install special lib for Horizont: ERROR."
    exit
fi
echo "+ Install special library for Horizont: complete."
# >>>> Add rsync port to reserve <<<<
sh -c "echo '873     # rsync' >> /etc/bindresvport.blacklist"
if [ "$?" -ne "0" ]; then
    echo "=!!= Add rsync port to reserve: ERROR."
    exit
fi
echo "+ Reserve rsync port: complete."
# >>>> Configure log for Horizont <<<<
sh -c "echo '# Horizont log' >> /etc/rsyslog.conf" &&
sh -c "echo 'user.*     -/var/log/user/user.log' >> /etc/rsyslog.conf"
if [ "$?" -ne "0" ]; then
    echo "=!!= Configure rsyslog: ERROR."
    exit
fi
echo "+ Configure rsyslog: complete."
# >>>> Make folder for Horizont log and give him own <<<<
mkdir /var/log/user
if [ "$?" -ne "0" ]; then
    echo "=!!= Create folder /var/log/user: ERROR."
    exit
fi
chown -R horizont:horiz /var/log/user
if [ "$?" -ne "0" ]; then
    echo "=!!= Change own on folder /var/log/user: ERROR."
    exit
fi
echo "+ Change own on folder /var/log/user: complete."
# >>>> Configure logrotate for Horizont log <<<<
sh -c "echo '/var/log/user/user.log {
    daily
    nodateext
    maxage 100
    rotate 35
    compress
    missingok
    ifempty
    create
    postrotate
    service rsyslog restart > /dev/null
        rm -f /var/log/user/user.log.[2-9] /var/log/user/user.log.[1-9][0-9] \ 
                                      /var/log/user/user.log.[1-9][0-9][0-9]
      endscript
    }' >> /etc/logrotate.d/userlog"
if [ "$?" -ne "0" ]; then
    echo "=!!= Modify logrotate.d/userlog: ERROR."
    exit
fi
echo "+ Modify logrotate.d/userlog: complete."

logrotate -f /etc/logrotate.conf
if [ "$?" -ne "0" ]; then
    echo "=!!= Start logrotate with new config: ERROR."
    exit
fi
echo "+ Start logrotate with new config: complete."

sed -i 's/RSYNC_ENABLE=false/RSYNC_ENABLE=true/g' /etc/default/rsync
if [ "$?" -ne "0" ]; then
    echo "=!!= Set RSYNC_ENABLE parameter: ERROR."
    exit
fi
echo "+ Set RSYNC_ENABLE parameter: complete."

/etc/init.d/rsync restart
if [ "$?" -ne "0" ]; then
    echo "=!!= Rsync restart: ERROR."
    exit
fi

# >>>> Install Docker and Docker-Compose <<<<
curl https://get.docker.com > /home/install.sh
if [ "$?" -ne "0" ]; then
    echo "=!!= Get docker install.sh: ERROR."
    exit
fi
chmod +x /home/install.sh
if [ "$?" -ne "0" ]; then
    echo "=!!= Change +x mode on docker install.sh: ERROR."
    exit
fi

../install.sh

usermod -aG docker horizont
if [ "$?" -ne "0" ]; then
    echo "=!!= User horizont added in docker group: ERROR."
    exit
fi
echo "+ User horizont added in docker group: complete."
pip install docker-compose
if [ "$?" -ne "0" ]; then
    echo "=!!= Install docker-compose: ERROR."
    exit
fi
echo "+ Install docker-compose: complete."
# >>>> Bond configuration <<<<
echo "Do you want to make bond network? (yes/no)"
answer="yes"
read answer
if [ "$answer" = "yes" ]; then
    apt install --yes ifenslave
    if [ "$?" -ne "0" ]; then
        echo "=!!= Install ifenslave package: ERROR."
        exit
    fi
    ifconfig eth0 down
    ifconfig eth1 down
    /etc/init.d/networking stop
    while [ -z "$ip_address" ]
    do
      echo "Please, enter ip address:"
      read ip_address
      if [ -z "$ip_address" ]; then
          echo "(!) You did not enter ip address." 
      else echo "Entered address: $ip_address"
      fi
    done
    while [ -z "$netmask" ]
    do
      echo "Please, enter network mask:"
      read netmask
      if [ -z "$netmask" ]; then
          echo "(!) You did not enter network mask." 
      else echo "Entered address: $netmask"
      fi
    done
    while [ -z "$gateway" ]
    do
      echo "Please, enter gateway:"
      read gateway
      if [ -z "$gateway" ]; then
          echo "(!) You did not enter gateway." 
      else echo "Entered address: $gateway"
      fi
    done
    sh -c "echo '# This file generate by Horizont preparation script.
           source /etc/network/interfaces.d/*
           # The loopback network interface
           auto lo
           iface lo inet loopback

           auto bond0
           iface bond0 inet static
             address $ip_address
             netmask $netmask
             gateway $gateway
             slaves eth0 eth1
             bond-mode broadcast
             bond-miimon 100
             bond-downdelay 200
             bond-updelay 200' > /etc/network/interfaces"
    ifconfig eth0 up
    ifconfig eth1 up
    /etc/init.d/networking start
else echo "=--= Bond setup was skipped."
fi

# >>>> Install special tools <<<<
apt install -y nmap minicom
if [ "$?" -ne "0" ]; then
    echo "=!!= Install special tools: ERROR."
    exit
fi
echo "+ Install special tools: complete."

echo "========================================================="
echo "=_==_= Congratulations! .......................... =_==_="
echo "=_==_= System was already prepared for Horizont... =_==_="
echo "========================================================="
